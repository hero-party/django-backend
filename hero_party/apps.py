from django.apps import AppConfig


class HeroPartyConfig(AppConfig):
    name = 'hero_party'
