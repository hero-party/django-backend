from django.db import models

class Task(models.Model):
    created = models.DateTimeField(auto_now_add = True)
    modified = models.DateTimeField(auto_now = True)
    
    title = models.CharField(max_length = 120)
    notes = models.TextField(blank = True)
    completed = models.DateTimeField(default = None, null = True, blank = True)
    
    dueDateTime = models.DateTimeField(default = None, null = True, blank = True)
