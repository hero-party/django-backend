from django.contrib import admin
from .models import Task

class HeroPartyAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'notes', 'completed', 'dueDateTime')

# Register your models here.
admin.site.register(Task, HeroPartyAdmin)
