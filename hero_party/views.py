from rest_framework import viewsets
from datetime import datetime
import logging

from .serializers import TaskSerializer
from .models import Task

logger = logging.getLogger(__name__)

class TaskView(viewsets.ModelViewSet):
    serializer_class = TaskSerializer
    queryset = Task.objects.all()
    
    def update(self, request, *args, **kwargs):
        
        if isinstance(completedBool := request.data['completed'], bool):
            request.data['completed'] = datetime.now() if completedBool else None
        
        logger.info(request.data)
        return super().update(self. request, args, kwargs)